" This plugin is automatically
" generate a table of markdown for Vim.

" Define the command.
command! -nargs=0 MarkdownTable call mdtable#initialize()
