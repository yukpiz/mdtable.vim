## About

This plugin is automatically generate a table of markdown for Vim.

*Requirements:*  

* Vim 7.4 patch 1-335 above.

*Dependent:*  

* None

## Installing

**For Vundle**  
~~~
# write in vimrc.
Bundle "https://yukpiz@bitbucket.org/yukpiz/mdtable.vim.git"
# run command in Vim.
:BundleInstall
~~~

**For NeoBundle**  
~~~
# write in vimrc.
NeoBundle "https://yukpiz@bitbucket.org/yukpiz/mdtable.vim.git"
# run command in Vim.
:NeoBundleInstall
~~~

## Usage
~~~
# in Vim.
:MarkdownTable
> Please enter the row count:
> Please enter the column count:
> Please enter the type[short(s)/middle(m)/long(l)/{Int}]:

# Type is [short/middle/long/{Int}]

:MarkdownTable
> Please enter the row count:4
> Please enter the column count:5
> Please enter the type[short(s)/middle(m)/long(l)/{Int}]:short
|       |       |       |       |       |
| ----- | ----- | ----- | ----- | ----- |
|       |       |       |       |       |
|       |       |       |       |       |
|       |       |       |       |       |

:MarkdownTable
> Please enter the row count:3
> Please enter the column count:4
> Please enter the type[short(s)/middle(m)/long(l)/{Int}]:middle
|            |            |            |            |
| ---------- | ---------- | ---------- | ---------- |
|            |            |            |            |
|            |            |            |            |

:MarkdownTable
> Please enter the row count:2
> Please enter the column count:3
> Please enter the type[short(s)/middle(m)/long(l)/{Int}]:long
|                 |                 |                 |
| --------------- | --------------- | --------------- |
|                 |                 |                 |

:MarkdownTable
> Please enter the row count:4
> Please enter the column count:5
> Please enter the type[short(s)/middle(m)/long(l)/{Int}]:2
|    |    |    |    |    |
| -- | -- | -- | -- | -- |
|    |    |    |    |    |
|    |    |    |    |    |
|    |    |    |    |    |
~~~

## Settings
~~~
# write in vimrc.
let g:mdtable#width_short = 15 "short type
let g:mdtable#width_middle = 20 "middle type
let g:mdtable#width_long = 25 "long type
~~~


## Format
|One    |Two    |Three  |Four   |Five   |
| ----- | ----- | ----- | ----- | ----- |
|hoge1  |hoge2  |hoge3  |hoge4  |hoge5  |
|hoge1  |hoge2  |hoge3  |hoge4  |hoge5  |
|hoge1  |hoge2  |hoge3  |hoge4  |hoge5  |
