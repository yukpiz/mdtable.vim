" This plugin is automatically
" generate a table of markdown for Vim.

function! mdtable#initialize() abort

    try

        let rowcount = mdtable#rowcount_operation()
        let columncount = mdtable#columncount_operation()
        let type = mdtable#type_operation()

        call mdtable#arguments_check(rowcount, columncount, type)

        let s:TableFormat = []
        call mdtable#emptyrow_create()
        call mdtable#linerow_create()
        let s:RowCount = s:RowCount - 1
        let l:rowidx = 0
        while l:rowidx < s:RowCount
            call mdtable#emptyrow_create()
            let l:rowidx = l:rowidx + 1
        endwhile

        call append(".", s:TableFormat)

    catch /^TypeException$/
        echo "TypeException: Type is [short/middle/long/{Int}]"
        return
    catch /^RowException$/
        echo "RowException: Row count is integer."
        return
    catch /^ColumnException$/
        echo "ColumnException: Column count is integer."
        return
    endtry

endfunction

function! mdtable#rowcount_operation()

    let inputtext = input("Please enter the row count:")
    echo "\n"
    return inputtext

endfunction

function! mdtable#columncount_operation()

    let inputtext = input("Please enter the column count:")
    echo "\n"
    return inputtext

endfunction

function! mdtable#type_operation()

    let inputtext = input("Please enter the type".
                \"[short(s)/middle(m)/long(l)/{Int}]:")
    echo "\n"
    return inputtext

endfunction

function! mdtable#arguments_check(row, column, type)

    let pattern = "\^[0-9]*$"
    if matchlist(a:row, pattern) == []
        throw "RowException"
    else
        let s:RowCount = a:row
    endif

    if matchlist(a:column, pattern) == []
        throw "ColumnException"
    else
        let s:ColumnCount = a:column
    endif

    if a:type == "short" || a:type == "s"
        if !exists("g:mdtable#width_short")
            let g:mdtable#width_short = 5
        endif
        let s:ColumnWidth = g:mdtable#width_short + 2
    elseif a:type == "middle" || a:type == "m"
        if !exists("g:mdtable#width_middle")
            let g:mdtable#width_middle = 10
        endif
        let s:ColumnWidth = g:mdtable#width_middle + 2
    elseif a:type == "long" || a:type == "l"
        if !exists("g:mdtable#width_long")
            let g:mdtable#width_long = 15
        endif
        let s:ColumnWidth = g:mdtable#width_long + 2
    elseif matchlist(a:type, pattern) != []
        let s:ColumnWidth = a:type + 2
    else
        throw "TypeException"
    endif

endfunction

function! mdtable#emptyrow_create()

    let l:rowchar = mdtable#rowchar_create(" ")
    let l:colidx = 0
    let l:linechar = "|"
    while l:colidx < s:ColumnCount
        let l:linechar = l:linechar . l:rowchar . "|"
        let l:colidx = l:colidx + 1
    endwhile

    call add(s:TableFormat, l:linechar)

endfunction

function! mdtable#rowchar_create(char)

    let l:colidx = 0
    let l:rowchar = " "

    while l:colidx < s:ColumnWidth - 2
        let l:rowchar = l:rowchar . a:char
        let l:colidx = l:colidx + 1
    endwhile

    let l:rowchar = l:rowchar . " "

    return l:rowchar

endfunction

function! mdtable#linerow_create()

    let l:rowchar = mdtable#rowchar_create("-")
    let l:colidx = 0
    let l:linechar = "|"
    while l:colidx < s:ColumnCount
        let l:linechar = l:linechar . l:rowchar . "|"
        let l:colidx = l:colidx + 1
    endwhile

    call add(s:TableFormat, l:linechar)

endfunction
